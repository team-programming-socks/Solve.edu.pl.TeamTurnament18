num = input()
data = input().split()

iterations = 0
for i, element in enumerate(data):
    element = int(element) - 1
    if (int(data[element]) - 1) != i:
        iterations += 1
if iterations == 0:
    iterations = 1
print(iterations)
