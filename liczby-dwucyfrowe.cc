#include <stdio.h>

#ifndef LOG
	#define log if(0)
#else
	#define log if(1)
#endif

#if 1

const int step = 10;

int count(unsigned long long v, int a, int b) {
	int out = 2;
	unsigned long long t = 0, t2 = 0;
	unsigned long long n = 1;
	unsigned long stepb = 0;
	unsigned long stepn = 1;
	for (int i = 0; i < step; i++) {
		stepb *= 10;
		stepn *= 10;
		stepb += b;
	}

	while (t*stepn + stepb < v) { //prevent a int overflow
		t *= stepn;
		t += stepb;
		n *= stepn;
		out += 2*step;
		log printf("b>> %lli %i\n", t, out);
	}

	while (t*10 + b < v) {
		t *= 10;
		t += b;
		n *= 10;
		out += 2;
		log printf("b>  %lli %i\n", t, out);
	}

	while (t > v) {
		n /= 10;
		t /= 10;
		out -= 2;
		log printf("b<  %lli %i\n", t, out);
	}

	if (a == 0) {
		return out;
	}

	unsigned long long post_t_v = (v-t)/n;

	log printf("= %lli %lli\n", v-t, post_t_v);

	while (post_t_v > t2*10+a ) { // there hopefully wont be a int overflow here
		t2 *= 10;
		t2 += a;
		out += 1;
		log printf("a>  %lli %i\n", t2, out);
	}
	return out;
}

int main() {
	unsigned long long N;
	int a,b;
	scanf("%lli %i %i", &N, &a, &b);
	
	if (N < a) {
		printf("0\n");
		return 0;
	}

	printf("%i", count(N, a, b));
}

#else

int main() {
	size_t buf_size = 19;
	char *buf = new char[buf_size];

	size_t digits = getline(&buf, &buf_size,stdin);

	int tot = 0;

	char *ptr = buf;

	while (*ptr != ' ') {
		if (*ptr >= buf[digits-2]) {
			log printf("%c >= %c\n", *ptr, buf[digits-2]);
			tot += 1;
		}
		if (*ptr >= buf[digits-4]) {
			log printf("%c >= %c\n", *ptr, buf[digits-4]);
			tot += 1;
		}
		ptr ++;
	}
	

	printf("%i", tot);
}

#endif
