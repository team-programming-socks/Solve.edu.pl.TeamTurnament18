Solution files for our entry into solve.edu.pl's 18th team turnament.

Each problem had its own branch to ease colabortion during the competition.

# Results:

- pary (A): Natalie (@cCodeKat) got close, but couldn't get there in time
- liczby-dwucyfrowe (F): Lily (@MinekPo1) got stuck on an edge case
- podizal-lupow (G): Natalie (@cCodeKat) got it done (our only point)

Every other task did not have an attempted solution with enough progress to be commited.
