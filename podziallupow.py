def BackpackOpenCalc(coin_sum):
    if coin_sum % 3 != 0:
        return "NIE"
    third = coin_sum // 3

    if data[0] == third:
        if data[1] == third:
            return 0
        return 2
    elif data[1] == third:
        if data[0] == third:
            return 0
        return 2
    elif data[2] == third:
        if data[1] == third:
            return 0
        return 2
    return 3

data = input().split()
data[0], data[1], data[2] = int(data[0]), int(data[1]), int(data[2])
print(BackpackOpenCalc(sum(data)))